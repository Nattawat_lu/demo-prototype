#include "i2c_01.h"
/*HAL_StatusTypeDef HAL_I2C_DeInit(I2C_HandleTypeDef *hi2c)
{
  if (hi2c == ((void *)0))
  {
    return HAL_ERROR;
  }
  ((void)0U);
  hi2c->State = HAL_I2C_STATE_BUSY;
  (((hi2c)->Instance->CR1) &= ~((0x1UL << (0U))));
  HAL_I2C_MspDeInit(hi2c);
  hi2c->ErrorCode = 0x00000000U;
  hi2c->State = HAL_I2C_STATE_RESET;
  hi2c->PreviousState = ((uint32_t)(HAL_I2C_MODE_NONE));
  hi2c->Mode = HAL_I2C_MODE_NONE;
  do{ (hi2c)->Lock = HAL_UNLOCKED; }while (0U);
  return HAL_OK;
}*/



HAL_StatusTypeDef HAL_I2C_Init(I2C_HandleTypeDef *hi2c)
{
  uint32_t freqrange;
  uint32_t pclk1;
  if (hi2c == ((void *)0))
  {
    return HAL_ERROR;
  }
 /* ((void)0U);
  ((void)0U);
  ((void)0U);
  ((void)0U);
  ((void)0U);
  ((void)0U);
  ((void)0U);
  ((void)0U);
  ((void)0U);*/
  if (hi2c->State == HAL_I2C_STATE_RESET)
  {
    hi2c->Lock = HAL_UNLOCKED;
    HAL_I2C_MspInit(hi2c);
  }
  hi2c->State = HAL_I2C_STATE_BUSY;
  (((hi2c)->Instance->CR1) &= ~((0x1UL << (0U))));
  pclk1 = HAL_RCC_GetPCLK1Freq();
  if ((((hi2c->Init.ClockSpeed) <= 100000U) ? ((pclk1) < 2000000U) : ((pclk1) < 4000000U)) == 1U)
  {
    return HAL_ERROR;
  }
  freqrange = ((pclk1)/1000000U);
  (((hi2c->Instance->CR2)) = ((((((hi2c->Instance->CR2))) & (~((0x3FUL << (0U))))) | (freqrange))));
  (((hi2c->Instance->TRISE)) = ((((((hi2c->Instance->TRISE))) & (~((0x3FUL << (0U))))) | ((((hi2c->Init.ClockSpeed) <= 100000U) ? ((freqrange) + 1U) : ((((freqrange) * 300U) / 1000U) + 1U))))));
  (((hi2c->Instance->CCR)) = ((((((hi2c->Instance->CCR))) & (~(((0x1UL << (15U)) | (0x1UL << (14U)) | (0xFFFUL << (0U)))))) | ((((hi2c->Init.ClockSpeed) <= 100000U)? ((((((((((pclk1))) - 1U)/((((hi2c->Init.ClockSpeed))) * (2U))) + 1U) & (0xFFFUL << (0U))) < 4U)? 4U:(((((((pclk1))) - 1U)/((((hi2c->Init.ClockSpeed))) * (2U))) + 1U) & (0xFFFUL << (0U))))) : ((((((hi2c->Init.DutyCycle)) == 0x00000000U)? (((((((pclk1))) - 1U)/((((hi2c->Init.ClockSpeed))) * (3U))) + 1U) & (0xFFFUL << (0U))) : ((((((((pclk1))) - 1U)/((((hi2c->Init.ClockSpeed))) * (25U))) + 1U) & (0xFFFUL << (0U))) | (0x1UL << (14U)))) & (0xFFFUL << (0U))) == 0U)? 1U : ((((((hi2c->Init.DutyCycle)) == 0x00000000U)? (((((((pclk1))) - 1U)/((((hi2c->Init.ClockSpeed))) * (3U))) + 1U) & (0xFFFUL << (0U))) : ((((((((pclk1))) - 1U)/((((hi2c->Init.ClockSpeed))) * (25U))) + 1U) & (0xFFFUL << (0U))) | (0x1UL << (14U))))) | (0x1UL << (15U))))))));
  (((hi2c->Instance->CR1)) = ((((((hi2c->Instance->CR1))) & (~(((0x1UL << (6U)) | (0x1UL << (7U)))))) | ((hi2c->Init.GeneralCallMode | hi2c->Init.NoStretchMode)))));
  (((hi2c->Instance->OAR1)) = ((((((hi2c->Instance->OAR1))) & (~(((0x1UL << (15U)) | 0x00000300U | 0x000000FEU | (0x1UL << (0U)))))) | ((hi2c->Init.AddressingMode | hi2c->Init.OwnAddress1)))));
  (((hi2c->Instance->OAR2)) = ((((((hi2c->Instance->OAR2))) & (~(((0x1UL << (0U)) | (0x7FUL << (1U)))))) | ((hi2c->Init.DualAddressMode | hi2c->Init.OwnAddress2)))));
  (((hi2c)->Instance->CR1) |= ((0x1UL << (0U))));
  hi2c->ErrorCode = 0x00000000U;
  hi2c->State = HAL_I2C_STATE_READY;
  hi2c->PreviousState = ((uint32_t)(HAL_I2C_MODE_NONE));
  hi2c->Mode = HAL_I2C_MODE_NONE;
  return HAL_OK;
}




